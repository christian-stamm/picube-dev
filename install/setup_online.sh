#!/bin/bash

# Exit on error
set -e

#----------------------------------------------------------
#-------------------- Network Setup -----------------------
#----------------------------------------------------------

network-setup()
{
    echo "Setup Network Config"
    # Move config to system path
    sudo cp network_config.yaml /etc/netplan/50-cloud-init.yaml
    # Update system config
    sudo netplan --debug generate
    sudo netplan --debug apply
}

#----------------------------------------------------------
#---------------------- SSH Setup -------------------------
#----------------------------------------------------------

ssh-setup()
{
    echo "Setup SSH Keys"
    # Check if ssh key already set
    if test -f "$HOME/.ssh/id_rsa"; then
        echo "Setup SSH Keys already exist"
    else
        # Create new key (no passwd and default ssh path)
        ssh-keygen -t rsa -b 2048 -N "" -f $HOME/.ssh/id_rsa
    fi

    echo "Change Root password"
    # Update password for user:passwd
    echo "root:rootlevel" | sudo chpasswd
    # Allow root ssh login
    echo "Setup Root SSH Connection"
    if [[ -v PERMIT_ROOT_LOGIN ]]; then
         echo "Root login already set"
    else
        echo "export PERMIT_ROOT_LOGIN=True" >> ~/.bashrc
        export PERMIT_ROOT_LOGIN=True
        echo "PermitRootLogin Yes" | sudo tee -a /etc/ssh/sshd_config
    fi
    
    # Apply ssh settings
    sudo systemctl restart ssh
}

#network-setup
ssh-setup
echo "Online setup done."
