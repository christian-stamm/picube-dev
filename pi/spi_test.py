import spidev
import time
import sys

def main() -> int:
    
    print("System initialization.")
    spi = spidev.SpiDev(0,0)
    spi.max_speed_hz = int(18e6)

    transfer_size = 10
    sum_errors = 0
    max_iterations = int(1e2)

    spi.xfer([tx for tx in range(transfer_size)])
    time.sleep(1)

    try:
        
        for it in range(0, max_iterations):
            
            rx_packet = spi.xfer([tx for tx in range(transfer_size)])
            
            print("Start packet check.")

            for idx, received_value in enumerate(rx_packet):
                expected_value = 255 - idx
                is_valid = (expected_value == received_value)

                print(f"Packet ID {idx} | Valid={is_valid} | Expected={expected_value} | Received={received_value}")

                if not is_valid:
                    sum_errors += 1
                
            print("Packet check finished.")
            print(f"Until loop {it+2} a total of {sum_errors} errors occured.")
            
            time.sleep(1e-2)

    except KeyboardInterrupt:
        print(f"User interrupt. Init cleanup.")
    
    spi.close()
    print("System shutdown.")
    return 0

if __name__ == '__main__':
    sys.exit(main())