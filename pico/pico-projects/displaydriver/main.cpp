#include <cstdio>
#include "pico.h"
#include "pico/stdio.h"
#include "pico/stdlib.h"
#include "pico/multicore.h"

#include "types.hpp"
#include "pixel.hpp"
#include "pixelloader.hpp"
#include "pixeldriver.hpp"

bool picoEnabled = false;
PixelLoader loader;
PixelDriver driver;
PixelBuffer pixels;

void core1_entry() 
{    
    while(picoEnabled)
    {
        driver.updatePio();
    }    
}

void core0_entry() 
{   
    while(picoEnabled)
    {
        loader.load(pixels);
    }
}


int main()
{
    stdio_init_all();

    sleep_ms(2000);
    printf("System Reset...\n");

    pixels.resize(8);

    for(Pixel &pixel : pixels)
    {
        if(pixel.getIndex())
        {
            pixel.setPosition(Position(16,16,16));
        }
    }

    pixels.at(0) = Pixel(Position(0, 0, 0), Color(0b000000010000, 0, 0));
    pixels.at(1) = Pixel(Position(0, 0, 1), Color(0b000000001111, 0, 0));
    pixels.at(2) = Pixel(Position(0, 0, 2), Color(0b000000001000, 0, 0));
    pixels.at(3) = Pixel(Position(0, 0, 3), Color(0b000000000111, 0, 0));
    pixels.at(4) = Pixel(Position(0, 0, 4), Color(0b000000000100, 0, 0));
    pixels.at(5) = Pixel(Position(0, 0, 5), Color(0b000000000010, 0, 0));
    pixels.at(6) = Pixel(Position(0, 0, 6), Color(0b000000000011, 0, 0));
    pixels.at(7) = Pixel(Position(0, 0, 7), Color(0b000000000001, 0, 0));
    
    sleep_ms(2000);
    printf("System Setup completed.\n");
    picoEnabled = true;

    multicore_launch_core1(core1_entry); core0_entry();
    return 0;
}