#include <cmath>
#include <string>
#include <sstream> 
#include "pixel.hpp"

#define DITHERMASK (((1 << PYHSICAL_BITS) - 1) << VIRTUAL_BITS)
#define MAXERROR ((1 << VIRTUAL_BITS) - 1)
#define MAXBRIGHTNESS ((1 << (PYHSICAL_BITS + VIRTUAL_BITS)) - 1)

#define CLAMP(min, max, value) (((value) > (max)) ? (max) : (((value) < (min)) ? (min) : (value)))

Pixel::Pixel()
    : Pixel(Position(0,0,0), Color(0,0,0))
{

}

Pixel::Pixel(const Position &position, const Color &color)
    : index{0}
    , enabled{1}
    , flags{0}
    , colorRed{color[0]}
    , errorRed{0}
    , colorGreen{color[1]}
    , errorGreen{0}
    , colorBlue{color[2]}
    , errorBlue{0}
{
    setPosition(position);
}

void Pixel::toggle()
{
    enabled = !enabled;
}

void Pixel::enable()
{
    enabled = true;
}

void Pixel::disable()
{
    enabled = false;
}

bool Pixel::isEnabled() const
{
    return enabled;
}

void Pixel::dither(Color &color)
{
    color[0]    = (uint16_t)(CLAMP(0, MAXBRIGHTNESS, colorRed   + errorRed)   & DITHERMASK);
    color[1]    = (uint16_t)(CLAMP(0, MAXBRIGHTNESS, colorGreen + errorGreen) & DITHERMASK);
    color[2]    = (uint16_t)(CLAMP(0, MAXBRIGHTNESS, colorBlue  + errorBlue)  & DITHERMASK);
    errorRed    = (uint16_t)(CLAMP(0, MAXERROR, errorRed   + colorRed   - color[0]));
    errorGreen  = (uint16_t)(CLAMP(0, MAXERROR, errorGreen + colorGreen - color[1]));
    errorBlue   = (uint16_t)(CLAMP(0, MAXERROR, errorBlue  + colorBlue  - color[2]));
    color[0]    >>= VIRTUAL_BITS;
    color[1]    >>= VIRTUAL_BITS;
    color[2]    >>= VIRTUAL_BITS;
}

void Pixel::setColor(const Color &color)
{
    colorRed = color[0];
    colorGreen = color[1];
    colorBlue = color[2];

    errorRed = 0;
    errorGreen = 0;
    errorBlue = 0;
}

void Pixel::getColor(Color &color) const
{
    if(enabled)
    {
        color[0] = colorRed;
        color[1] = colorGreen;
        color[2] = colorBlue;
    }
    else
    {
        color = Color::Zero();
    }
}

void Pixel::setPosition(const Position &position) 
{
    index = 256 * position[2] + 16 * position[1] + position[0];
}

void Pixel::getPosition(Position &Position) const
{
    Position[0] = (uint8_t)(index) % 16;
    Position[1] = (uint8_t)(index / 16) % 16;
    Position[2] = (uint8_t)(index / 256) % 16;
}

uint16_t Pixel::getIndex() const
{
    return index;
}

std::string Pixel::to_string() const
{
    Color col;
    Position pos;
    std::stringstream ss;
    Eigen::IOFormat CleanFmt(2, 0, ", ", ",", "", "", "[", "]");

    getColor(col);
    getPosition(pos);

    ss << "Position=" << pos.format(CleanFmt) << ", Color=" << col.format(CleanFmt) << ", enabled=" << enabled;
    return "Pixel(" + ss.str() + ")";
}