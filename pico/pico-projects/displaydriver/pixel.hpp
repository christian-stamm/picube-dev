#pragma once

#include "types.hpp"

#define VIRTUAL_BITS 4
#define PYHSICAL_BITS 8

class Pixel
{
    
public:

    Pixel();
    Pixel(const Position &position, const Color &color);

    void toggle();
    void enable();
    void disable();
    bool isEnabled() const;

    void dither(Color &color);
    void setColor(const Color &color);
    void getColor(Color &color) const;
  
    void setPosition(const Position &position);
    void getPosition(Position &position) const;
    
    uint16_t getIndex() const;
    std::string to_string() const;

private:

    uint16_t index : 12;
    uint16_t enabled : 1;
    uint16_t flags : 3;
    uint16_t colorRed : 12;
    uint16_t errorRed : 4;
    uint16_t colorGreen : 12;
    uint16_t errorGreen : 4;
    uint16_t colorBlue : 12;
    uint16_t errorBlue : 4;
};