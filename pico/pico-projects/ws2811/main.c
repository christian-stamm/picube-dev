#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "ws2811.pio.h"

int main()
{
    PIO pio = pio0;

    // Find free location in PIO's instruction memory
    uint mem = pio_add_program(pio, &ws2811_program);

    // Find free state machine
    uint sm = pio_claim_unused_sm(pio, true);

    // Init Program
    ws2811_program_init(pio, sm, mem, 2);

    // The state machine is now running.
    while (true)
    {
        pio_sm_put_blocking(pio, sm, 0x08040201);
        pio_sm_put_blocking(pio, sm, 0x80402010);
        pio_sm_put_blocking(pio, sm, 0x08040201);
        pio_sm_put_blocking(pio, sm, 0x80402010);
        sleep_ms(1000);
    }
}